class Atoms.Organism.Map extends Atoms.Organism.Article

  @scaffold "assets/scaffold/map.json"

  constructor:()->
    super
    do @render

  onSearchSubmit: (event, search) ->
    event.preventDefault()
    @main.mymap.query search.value() if search.value()
    false

  onGMapQuery: (places) ->
    @main.mymap.clean()
    if places.length > 0
      @main.mymap.marker places[0].position
      @main.mymap.center places[0].position, zoom = 15
    false

