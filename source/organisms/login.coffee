"use strict"

class Atoms.Organism.Login extends Atoms.Organism.Article

  @url: "assets/scaffold/login.json"

  render: ->
    super
    do @onShowLogin
    @main.form.el.children().first().focus()

  # -- Children Bubble Events --------------------------------------------------

  onShowLogin: (event) =>
    @el.find(".signup-context").hide()
    @el.find(".login-context").show()
    @main.form.error.el.hide()

  onShowSignup: (event) ->
    @el.find(".login-context").hide()
    @el.find(".signup-context").show()
    @main.form.error.el.hide()

  onFormChange: (event, form) ->
    @main.form.error.el.hide()

  onLogin: (event, button) ->
    button.el.addClass("loading")
    Atoms.Url.path "task/main"
    #@_session "login", @main.form.value()

  onSignup: (event, button) ->
    button.el.addClass("loading")
    #@_session "signup", @main.form.value()

  onClose: ->
    do @hide
    false

  # -- Private -----------------------------------------------------------------
  # _session: (method, parameters, background = true) ->
  #   do @_disableForm
  #   __.proxy("POST", method, parameters, background).then (error, response) =>
  #     do @_enableForm
  #     if response
  #       document.cookie = "mentorcode=#{response.token}"
  #       window.location = "/"
  #     else
  #       @main.form.error.el.html(error.message).show()

  _disableForm: ->
    @main.form.el.children().attr "disabled", true

  _enableForm: ->
    @main.form.el.children().removeAttr("disabled").removeClass "loading"
