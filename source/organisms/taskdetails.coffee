class Atoms.Organism.Taskdetails extends Atoms.Organism.Dialog

  @scaffold "assets/scaffold/taskdetails.json"

  show: (@entity) ->
    super

    if @entity?
      #Edit task
      @main.form.input.value entity.text
      @main.form.description.value entity.description
      @main.form.switch.value entity.done
      @main.calendar.date new Date entity.date if entity.date
    else
      #New task
      do @main.form.clean
      do @main.form.refresh
      @main.calendar.date new Date

  onClose: ()->
    @main.form.clean()
    @main.form.refresh()
    @hide()

  onSave: (event, atom) =>
    values =
      text: @main.form.input.value()
      description: @main.form.description.value()
      done: @main.form.switch.value()
      date: new Date @main.calendar.current

    if @entity
      @entity.updateAttributes values
    else
      __.Entity.Task.create values
    @onClose()