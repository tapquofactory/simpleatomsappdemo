class Atoms.Organism.Task extends Atoms.Organism.Article

  @scaffold "assets/scaffold/task.json"


  onTask: () ->
    __.Dialog.Taskdetails.show()

  render: () ->
    super
    today = new Date()

    #Demo task generator
    for _i in [0..8] by 1
      defaultTask =
        text: "Demo task #{_i}"
        done: false
        description: "Task description #{_i}"
        date: today
      __.Entity.Task.create defaultTask