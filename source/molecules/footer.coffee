"use strict"

class Atoms.Molecule.FooterNav extends Atoms.Molecule.Navigation

  @extends: true

  @default:
    children: [
      "Atom.Button": icon: "note2", name:"task", path: "task/main"
    ,
      "Atom.Button": icon: "map-marker", name:"map", path: "map/main"
    ]