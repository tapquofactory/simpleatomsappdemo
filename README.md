# A simple atoms app

A simple atoms app demo made with Atoms IDE [Atoms]

![image](http://i45.photobucket.com/albums/f80/tapquofactory/demoappscreen_zps879010b8.jpg)



Install
--------------

Install all npm dependencies from package.json and run gulp:
```
npm install
gulp init
gulp

```


Gulp will start a server on port 8000 (the default port defined in gulpfile.js). Open your browser and type: localhost:8000 to try it.


[atoms]:http://atoms.tapquo.com/
[todomvc]:http://todomvc.com